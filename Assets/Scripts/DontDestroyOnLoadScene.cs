using UnityEngine;
using UnityEngine.SceneManagement;

public class DontDestroyOnLoadScene : MonoBehaviour
{

    public GameObject[] objects; //Variable qui définit les entitées dans les scènes unity.

    public static DontDestroyOnLoadScene instance; //Variable qui définit la non destruction d'object.

    private void Awake()
    {
        if (instance != null)//Si instance est nul
        {
            Debug.LogWarning("Il y a plus d'une instance de DontDestroyOnLoadScene dans la scène");// Envoie un message d'avertissement à l'utilisateur
            return;
            //On affche un message d'avertissement à l'utilisateur.
        }

        instance = this;

        foreach (var element in objects)
        {
            DontDestroyOnLoad(element);//On ne détruit pas l'élément.
        }
    }

    public void RemoveFromDontDestroyOnLoad() // Varaivle qui permet de déplacer les élément de DontDestroyOnLoad dans la scène 1 à chaque fois que on recharge la scène.
    {
        foreach (var element in objects)
        {
            SceneManager.MoveGameObjectToScene(element, SceneManager.GetActiveScene());// On recharge la scène actuel sur laquel on était.
        }
    }
}
