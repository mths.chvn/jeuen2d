using UnityEngine;

public class CurrentSceneManager : MonoBehaviour
{
    public bool isPlayerPresentByDefault = false; //Variable qui permet de savoir que la présence du personnage par défaut est fausse.
    public int coinsPickedUpInThisSceneCount; // Variable qui permet de savoir le nombre de pièce que le joueur possède dans la première scène.

    public static CurrentSceneManager instance;// Varaible qui permet de définir la gestion des scènes au moment de l'exécution.

    private void Awake()
    {
        if (instance != null) //Si Instance est null
        {
            Debug.LogWarning("Il y a plus d'une instance de CurrentSceneManager dans la scène"); //Un message dérreur apparait pour dire qu'il n'y a pas de CurrentSceneManager dans la scène.
            return;// On retourne le message d'avertissement à l'utilisateur. 
        }

        instance = this;
    }
}
