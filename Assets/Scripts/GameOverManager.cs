using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    public GameObject gameOverUI;//Variable qui définit l'écran de game over

    public static GameOverManager instance; //Variable qui définit l'instance du gameovermanager

    private void Awake()
    {
        if (instance != null)//Si instance est nul
        {
            Debug.LogWarning("Il y a plus d'une instance de GameOverManager dans la scène");//On transmet un message d'avertissement à l'utilisateur.
            return;
        }

        instance = this; //Instance est égal à this
    }

    public void OnPlayerDeath() //Variable qui définit la mort du joueur.
    {
        if(CurrentSceneManager.instance.isPlayerPresentByDefault)//Si l'instance du manager de la scène principale est par défault
        {
            DontDestroyOnLoadScene.instance.RemoveFromDontDestroyOnLoad();//On enleve tous les éléments de la scène.
        }

        gameOverUI.SetActive(true);//On active le menu game over
    }

    public void RetryButton()//Variable qui définit le bouton de retry
    {
        Inventory.instance.RemoveCoins(CurrentSceneManager.instance.coinsPickedUpInThisSceneCount);//On enleve tous les pièces de l'inventaire
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        PlayerHealth.instance.Respawn();//On fait réapparaitre le joueur
        gameOverUI.SetActive(false); //On désactive le menu game over
    }

    public void MainMenuButton()//Variable qui définit le bouton de menu principale
    {
        DontDestroyOnLoadScene.instance.RemoveFromDontDestroyOnLoad();//On enleve tous les éléments présents daans le scène.
        SceneManager.LoadScene("MainMenu");//On charge le menu principal
    }

    public void QuitButton()//Variable qui définit le boutonde fermeture du jeu
    {
        Application.Quit();//On quitte le jeu
    }
}