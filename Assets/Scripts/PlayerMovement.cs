using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed;// Variable public qui définit la vitesse de deplacement.
    public float climbSpeed; // Variable public qui définit la vitesse de montée.

    public float jumpForce; // Variable public qui définit la force du saut.

    private bool isJumping;// Variable privé qui permet de savoir si le personnage est entrain de faire l'action de sauter ou pas. 
    private bool isGrounded;// Varaible qui permet de savoir si le personnage est au sol ou pas.
    [HideInInspector] // La variable "isClimbing" disparait dans l'inspector.
    public bool isClimbing; // Varaible qui permet de savoir si le personnage est en train de montée ou pas.

    public Transform groundCheck;// Variable qui permet de savoir si le personnage est au sol ou pas.
    public float groundCheckRadius;// Variable qui permet de définir le rayon du cercle de groundcheck.
    public LayerMask collisionLayers;

    public Rigidbody2D rb;// Variable qui permet d'appliquer de la physique à un personnage ou un objet.'.    
    public Animator animator;//Variable qui permet d'animer un personnage.
    public SpriteRenderer spriteRenderer;// Variable qui permet de donner un sprite au personnage.
    public CapsuleCollider2D playerCollider; //Variable qui définit les collision en cercle du joueur.

    private Vector3 velocity = Vector3.zero; // Variable qui permet la représentation de vecteur et de point 3D.
    private float horizontalMovement;// Variable qui permet le déplacement du joueur à horizontal.
    private float verticalMovement; // Variable qui permet le déplacement du joueur à la vertical.

    public static PlayerMovement instance; // Variable qui permet le déplacement du joueur à horizontal.

    private void Awake() // Permet de savoir si le joueur est au sol et le faire sauter.
    {
        if (instance != null) // Si instance est nul
        {
            Debug.LogWarning("Il y a plus d'une instance de PlayerMovement dans la scène"); // On envoie un message d'avertissement à l'utilisateur.
            return;
        }

        instance = this;
    }

    void Update()
    {
        horizontalMovement = Input.GetAxis("Horizontal") * moveSpeed * Time.fixedDeltaTime; // Le mouvement horizontal est égal à la valeur de l'axe virtuel horizontal fois le vitesse de déplacement fois le temps au fils du temps.
        verticalMovement = Input.GetAxis("Vertical") * climbSpeed * Time.fixedDeltaTime; // Le mouvement vertical est égal à la valeur de l'axe virtuel vertical fois le vitesse de montéée du joueur fois le temps au fils du temps.

        if (Input.GetButtonDown("Jump") && isGrounded && !isClimbing) // Si la touche espace est actionner et que le personnage est au sol.
        {
            isJumping = true;//le personnage va sauté
        }
   

        Flip(rb.velocity.x); // On effectue un flip avec comme paramètre la revolicté du rigidbody sur x.

        float characterVelocity = Mathf.Abs(rb.velocity.x);
        animator.SetFloat("Speed", characterVelocity); // On envoie les valeurs floatants de "Speed" et de la velocité du joueur à l'animator.
        animator.SetBool("isClimbing", isClimbing); // On envoie les valeurs floatants de "isClimbing" à l'animator.
    }

    void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, collisionLayers);// IsGrounded est égal à la zone circulaire entre la position du groundcheck , le rayon de groundcheck et des layers de collision.Si la zone circulaire est touché isGrounded est égal à true donc le personnage est au sol sinon le personnage n'est pas au sol..
        MovePlayer(horizontalMovement, verticalMovement); //On fait un appel à la fonction afin de faire bouger le personnage de façon horizontal.
    }

    void MovePlayer(float _horizontalMovement, float _verticalMovement)
    {
        if (!isClimbing) // Si le joueur escalade un echelle
        {
            Vector3 targetVelocity = new Vector2(_horizontalMovement, rb.velocity.y); // Le vecteur se nommant "la velocité de la cible" est égal à un nouveau vecteur avec les paramètres le mouvement horizontal et le mouvement vertical du rigidbody .La direction vers laquel on va se dirigé est basé sur le mouvement horizontal et le mouvement vertical (y) 
            rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, .05f);// La vitesse du regidbody est égal au déplacement fondu du vecteur.Le SmoothDamp se fera entre la velociter du rigidbody et le velociter de la cible à atteindre. 

            if (isJumping) // Si le joueur est en train de sauté
            {
                rb.AddForce(new Vector2(0f, jumpForce)); //On va ajouter une force sur l'axe horizontal à notre joueur avec en x acune foce et en y la force de saut.
                isJumping = false;// Donc on est plus entrain de sauter.
            }
        }
        else //Sinon
        {
            Vector3 targetVelocity = new Vector2(0, _verticalMovement); //Le vecteur 3 est égal à un nouveau vecteur avec les paramètres 0 et mouvement vertical.
            rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, .05f);// La vitesse du regidbody est égal au déplacement fondu du vecteur.Le SmoothDamp se fera entre la velociter du rigidbody et le velociter de la cible à atteindre. 

        }
        
    }

    void Flip(float _velocity)
    {
        if (_velocity > 0.1f) //Si la velocité est supérieur à 0.1
        {
            spriteRenderer.flipX = false;  //On ne fait pas un flip du srite renderer
        }else if(_velocity < -0.1f)// Si le velocité est inferieur a -0.1
        {
            spriteRenderer.flipX = true;//On fait un flip du srite renderer
        }
    }

    private void OnDrawGizmos()// Variable qui permet de sélectionner rapidement des objets importants dans la scène
    
    {
        Gizmos.color = Color.red;// La sphére va etre de couleur jaune.
        Gizmos.DrawWireSphere(groundCheck.position, groundCheckRadius);// Dans Gizmos, on va crée une sphére avec la position du point sur le sol et de son rayon.
    }
}
