using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public string levelToLoad;

    public GameObject settingsWindow;//Variable qui définit la fenetre de paramètres

    public void StartGame() // Variable qui  définit le bouton de départ
    {
        SceneManager.LoadScene(levelToLoad); //On charge la première scène du jeu
    }

    public void SettingsButton() //Variable qui définit l'ouverture des paramètres d'affichages
    {
        settingsWindow.SetActive(true);//On affiche les diffèrents paramètres d'affichages
    }

    public void CloseSettingsWindow()// Variable qui définit la fermeture des paramètres d'affichages
    {
        settingsWindow.SetActive(false); //On n'affiche pas les diffèrents paramètres d'affichages
    }
    public void LoadCreditScene()//Varaible qui permet de définir le chargement de la scène des crédits
    {
        SceneManager.LoadScene("Credit");//On charge la scène des crédits.
    }

    public void QuitGame()// Variable qui définit la sortie du jeu
    {
        Application.Quit();// On quitte le jeu 
    }
}
