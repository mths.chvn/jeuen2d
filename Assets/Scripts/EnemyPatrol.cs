using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    public float speed;// variable qui permet de connaitre la vitesse de l'ennemie
    public Transform[] waypoints;//Variable qui permet de déplacer un ennemi entre deux points.

    public int damageOnCollision = 20;//Variable qui permet de donner des points de dommages à l'ennemie

    public SpriteRenderer graphics;// Variable qui permet de donner un sprite à un personnage.
    private Transform target;// Variable qui permet à un ennemie de ce dirigé vers un point
    private int destPoint = 0;// Variable qui permet de donner le point de destination de l'ennemi

    void Start()
    {
        target = waypoints[0];// La target est égal au point 0
    }

    void Update()
    {
        Vector3 dir = target.position - transform.position; // La direction de l'ennemi est égal à la position actuel de l'ennemi moins la postion à atteindre.Cela nous permet de savoir dans quel direction aller pour le prochaine point.
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);// On effectue le deplacement  de l'ennemie en multiplant la direction normalisé avec la vitesse et le déplacement en fonction du temps. Le personnage se deplacera en fonction du monde.

         //Si la distance entre la position de l'ennemie et le point a atteindre est inferieur à 0.3, on passe alors au point suivant.
        if(Vector3.Distance(transform.position, target.position) < 0.3f)// Si la distance entre la position de notre objet actuel et la position du point a atteindre est inferieur à 0.3
        {
            destPoint = (destPoint + 1) % waypoints.Length;// le point vers lequel on se dirige est égale au point premier point  plus 1 et à la longeur entre le premier et le dernier point.
            target = waypoints[destPoint];// le point cible est egal au prochain point.
            graphics.flipX = !graphics.flipX;// Si actuellement le sriterenderer "graphics" est flipé, on va le défliper. Et s'il est déflipé, on va le fliper.
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) //Variable qui permet de savoir si le joueur est rentre en collision avec l'ennemi.
    {
        if (collision.transform.CompareTag("Player")) //Variable qui permet de savoir si le joueur est rentre en collision avec l'ennemi.
        {
            PlayerHealth playerHealth = collision.transform.GetComponent<PlayerHealth>();// On va récupéré le script playerHealth.
            playerHealth.TakeDamage(damageOnCollision);//On fait appel à la variable playerHealth et on donne des point de dégats à la vie du joueur.
        }
    }
}
//Ce script permet de faire bouger l'ennemie ainsi que de donner des dégats au joueur.