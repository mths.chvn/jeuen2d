using UnityEngine;
using System.Collections; //Bibliothèque qui permet l'utilisation des corrutines
using UnityEngine.SceneManagement;

public class LoadSpecificScene : MonoBehaviour
{
    public string sceneName; // Variable qui permet de charger la scène suivante
    public Animator fadeSystem; // Variable qui permet de faire référence à l'animator

    private void Awake()
    {
        fadeSystem = GameObject.FindGameObjectWithTag("FadeSystem").GetComponent<Animator>(); // FadeSystem est égal au game object portant la marque "player.
    }

    private void OnTriggerEnter2D(Collider2D collision) // Variable qui permet d'activer la scène suivant si le joueur entre en collision avec la porte.
    {
        if(collision.CompareTag("Player")) // Si le joueur entre en collision
        {
            StartCoroutine(loadNextScene());//On passe à la scene suivante
        }
    }

    public IEnumerator loadNextScene() // Corrutine qui permet de charger la scène suivante avec un fondu visuel
    {
        fadeSystem.SetTrigger("FadeIn"); // On effectue un appel à notre trigger "FadeIn".
        yield return new WaitForSeconds(1f);// On fait pacientier le joueur 1 seconde.
        SceneManager.LoadScene(sceneName);// On charge la scene suivante.
    }
}
