using UnityEngine;

public class HealPowerUp : MonoBehaviour
{
   public int healthPoint;
   public AudioClip pickupSound;
   private void OnTriggerEnter2D(Collider2D collision)//Variable qui permet de savoir si un objet ou le joueur entre en collision avec un object
    {
        if (collision.CompareTag("Player")) //Si le joueur entre en collision
        {
            if(PlayerHealth.instance.currentHealth != PlayerHealth.instance.maxHealth)//Si la vie actuelle du joueur est égal à la vie max du joueur.
            {
               AudioManager.instance.PlayClipAt(pickupSound, transform.position); //On accède à l'audio manager afin de permettre de jouer un son
               PlayerHealth.instance.HealPlayer(healthPoint);//On ne peut pas prendre d'object régénérateur
               Destroy(gameObject);//On détruit l'object.
            }
           
        }
    }
}
//Ce script permet au joueur de regagner de la vie s'il a subit des dégats.