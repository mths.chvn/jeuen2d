using UnityEngine;
using System.Collections;

public class DeathZone : MonoBehaviour
{
    private Transform playerSpawn;
    private Animator fadeSystem; // Variable qui permet de faire appel à l'animator.

    private void Awake()
    {
        playerSpawn = GameObject.FindGameObjectWithTag("PlayerSpawn").transform; // Player Spawn est égal au résultat du composant associé au tag "PlayerSpawn".
        fadeSystem = GameObject.FindGameObjectWithTag("FadeSystem").GetComponent<Animator>(); // Fade System est égal au résultat du composant associé au tag "fadeSystem".
    }

    private void OnTriggerEnter2D(Collider2D collision)//Variable qui permet de savoir si le joueur est rentre en collision avec l'ennemi.
    {
        if (collision.CompareTag("Player")) // Si le joueur entre en collision.
        {
            StartCoroutine(ReplacePlayer(collision));// La coroutine Collision démarre.
        }
    }

    private IEnumerator ReplacePlayer(Collider2D collision)
    {
        fadeSystem.SetTrigger("FadeIn");// Définit la valeur du paramètre de déclenchement donné
        yield return new WaitForSeconds(1f);//On attend une seconde
        collision.transform.position = playerSpawn.position;//Les positions des collisions sont égal aux positions du playerSpawn.
    }
}
//Ce script permet de mettre en place un zone de mort dans la scène.