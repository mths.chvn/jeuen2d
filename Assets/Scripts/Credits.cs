using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour
{
   public void LoadMainMenu()
   {
       SceneManager.LoadScene("MainMenu"); //Charge le menu principale 
   }
    private void Update()
   {
       if(Input.GetKeyDown(KeyCode.Escape))
       {
           LoadMainMenu();
       }
   }
}
