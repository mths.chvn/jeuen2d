using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using System.Linq;

public class Setting : MonoBehaviour
{
    public AudioMixer audioMixer;

    public Dropdown resolutionDropdown;

    Resolution[] resolutions;

    

    public void Start()
    {
        

        resolutions = Screen.resolutions.Select(resolution => new Resolution { width = resolution.width, height = resolution.height }).Distinct().ToArray();
        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>(); //La liste des options est égal à un nouvelle liste avec des éléments.

        int currentResolutionIndex = 0;// L'index de la resolution actuel est égal à 0
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height; //Les options sont égal à la largeur de la resolution plus x plus la taille de la resolution.
            options.Add(option);//On ajoute les diffèrentes resolutions.

            if(resolutions[i].width == Screen.width && resolutions[i].height == Screen.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options);//On ajoute des diffèrents resolutions.
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();// Rafraichit la taille de l'écran du joueur.

        Screen.fullScreen = true;
    }

    public void SetVolume(float volume)//Varaible qui définti le réglage du son du jeu
    {
        audioMixer.SetFloat("Music", volume);//On peut regler le son en bougant le curseur sur le menu des settings.
    }
    public void SetSoundVolume(float volume)//Varaible qui définti le réglage du son du jeu
    {
        audioMixer.SetFloat("Sound", volume);//On peut regler le son en bougant le curseur sur le menu des settings.
    }


   
    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

    public void SetResolution(int resolutionIndex)//Variable qui définit les les diffèrents resolutions possibles.
    {
        Resolution resolution = resolutions[resolutionIndex]; // La résolution est égal à la résolution avec le paramétre resolution index
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen); //La résolution de l'écran à pour paramétre le taille de la résolution, la largeur de la resolution et le pleine écran. 
    }
}


//Ce script permet d'avoir des reglages dans le jeu.