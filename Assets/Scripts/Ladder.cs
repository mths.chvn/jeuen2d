using UnityEngine;
using UnityEngine.UI;

public class Ladder : MonoBehaviour
{
    private bool isInRange; // Variable booléene qui permet de savoir si le joueur est à proximité de l'echelle.
    // Start is called before the first frame update
    private PlayerMovement playerMovement;
    public BoxCollider2D topCollider; //Varaible qui definit le collider du haut de l'echelle
    private Text interactUI; //Variable qui définit l'interaction avec le texte
    void Awake()
    {
        playerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>(); // PlayerMovement est égal à l'objet possédant le tag "player" et qui a le component "playerMovement"
        interactUI = GameObject.FindGameObjectWithTag("InteractUI").GetComponent<Text>();// interactUIest égal à l'objet possédant le tag "interactUI" et qui a le component "text"
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isInRange && playerMovement.isClimbing && Input.GetKeyDown(KeyCode.Joystick1Button3)) // Si le joueur est a porté et qu'il est entrain de crimpé à l'échelle et qu'on appuie sur la touche E
        {
            // descendre de l'echelle
            playerMovement.isClimbing = false; // Le joueur ne monte plus à l'echelle
            topCollider.isTrigger = false; //Le collider du haut de l'echelle est désactivé
            return;
        }
        if(isInRange && Input.GetKeyDown(KeyCode.E))// Si le joueur est a porté et qu'on appuie sur la touche E
        {
            playerMovement.isClimbing = true; // Le joueur va monté à l'echelle
            topCollider.isTrigger = true;//Le collider du haut de l'echelle va etre activer.
        }
    }
    private void OnTriggerEnter2D(Collider2D collision) //Variable qui permet de lire du code lorsqu'un player entre dans la zone de collision
    {
        if(collision.CompareTag("Player")) // Si le joueur entre en collision
        {
            interactUI.enabled = true;//Le texte est activé
            isInRange = true;//Le joueur est a proximité
        }
    }
    private void OnTriggerExit2D(Collider2D collision)//Variable qui permet de lire du code lorsqu'un player sort de la zone de collision.
    {
       if(collision.CompareTag("Player")) // Si le joueur entre en collision
        {
            isInRange = false; //Le joueur n'est  pas a proximité
            playerMovement.isClimbing = false;//Le mouvement de monté du joueur est fausse
            topCollider.isTrigger = false;//Le collider du haut de l'echelle est désactivé
            interactUI.enabled = false; //Le texte est désactivé
        }
    }

}
//Ce script permet de monter et de désandre d'une échelle
