 using UnityEngine;

public class PickUpObject : MonoBehaviour
{
    
    public AudioClip sound;
    private void OnTriggerEnter2D(Collider2D collision)// Variable qui permet de savoir si le joueur entre en collision avec un object
    {
        if (collision.CompareTag("Player")) // Si le joueur entre en collision
        {
            AudioManager.instance.PlayClipAt(sound, transform.position);//On accède à l'audio manager afin de permettre de jouer un son
            Inventory.instance.AddCoins(1); //On ajoute une pièche à l'inventaire du joueur.
            CurrentSceneManager.instance.coinsPickedUpInThisSceneCount++;//On récupére le nombre de pièce dans la scène.
            Destroy(gameObject);//On détruit l'objet.
        }
    }
}
