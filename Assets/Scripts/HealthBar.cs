using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider slider; // Variable qui permet de créer un slider.

    public Gradient gradient;
    public Image fill; //Variable qui permet de connaitre le remplissage de la barre de vie.

    public void SetMaxHealth(int health) //  Variable qui permet l'initialisation de  la vie  du joueur.
    {
        slider.maxValue = health; //La vie max du joueur est egal à 100.
        slider.value = health;

        fill.color = gradient.Evaluate(1f); //La couleur de remplissage de la barre de vie est égal à la valeur égal à 1f.
    }

    public void SetHealth(int health) // Variable qui permet un certain nombre de point de vie au joueur.Elle permet de montrer au joueur ses point de vie 
    {
        slider.value = health;  //La valeur du slider est égal à la vie max.

        fill.color = gradient.Evaluate(slider.normalizedValue); //La couleur de remplissage de la barre de vie est égal à la valeur normalisé du silder.
    }
}
//Ce script permet au joueur d'avoir un barre de vie.