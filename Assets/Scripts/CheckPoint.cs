using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    private Transform playerSpawn;// Variable qui permet de définir la position, la rotation d'un objet.
    public void Awake()
    {
        playerSpawn = GameObject.FindGameObjectWithTag("PlayerSpawn").transform;//PlayerSpawn est égal auGameObject retacher au tag "PlayerSpawn".

    }
    private void OnTriggerEnter2D(Collider2D collision)//Variable qui permet de savoir si le joueur est rentre en collision avec l'ennemi.
    {
        if(collision.CompareTag("Player")) //Si le joueur entre en collision
        {
            playerSpawn.position = transform.position; // Les positions du spawn du joueur est égal au position de transform.
            gameObject.GetComponent<BoxCollider2D>().enabled = false; //Renvoie le composant du box collider du checkpoint.
        }
    }

}
//Ce scipt permet de mettre en place des checkpoints pour le joueur.
