using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player; // Variable qui permet de définir le GameObject du joueur.
    public float timeOffset;
    public Vector3 posOffset;

    private Vector3 velocity;//Variable qui permet de définir la vélocité du Vecteur.
    
    void Update()
    {
        transform.position = Vector3.SmoothDamp(transform.position, player.transform.position + posOffset, ref velocity, timeOffset); 
        //  Permet de modifie progressivement un vecteur vers un objectif souhaité au fil du temps.
    }
}
//Ce script permet de suivre le joeur dans la scène.
