using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{
    public int maxHealth = 100; // La vie maximal est égal à 100.
    public int currentHealth; //Variable qui donne le vie actuelle du joueur.

    public float invincibilityTimeAfterHit = 3f; // Variable qui permet de dire que lorsque le joueur est touché, il est invincible pendant 3 secondes.
    public float invincibilityFlashDelay = 0.2f; // Variable qui permet de dire que  le joueur est invincible pendant 2 secondes.
    public bool isInvincible = false; //Variable qui définit l'invincibilité du joueur

    public SpriteRenderer graphics; // Varialbe qui définir l'apparence du joueur.
    public HealthBar healthBar; //Variable qui permet de donner la barre de vie au joueur.
    public AudioClip hitSound;

    public static PlayerHealth instance;

    private void Awake()
    {
        if (instance != null) // Si instance est nul
        {
            Debug.LogWarning("Il y a plus d'une instance de PlayerHealth dans la scène"); //On voit un message d'avertissemtn à l'utilisateur
            return;
        }

        instance = this;
    }

    void Start()
    {
        currentHealth = maxHealth; // On dit que la vie actuelle est égal à la vie max
        healthBar.SetMaxHealth(maxHealth); // Le barre de vie est rempli avec la vie max et elle dessent à chaque fois que le joueur reçois des dégats.  
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H)) // SI le touche T du klavier est présser, le joueur prend des dégats
        {
            TakeDamage(20); // Le joueur prends 20 de dégats
        }
    }

    public void HealPlayer(int amount)
    {
        if((currentHealth + amount) > maxHealth) // Si la vie actuel plus le montant est supèrieur à la vie max
        {
            currentHealth = maxHealth; // la vie actuel est égal à la vie max.
        }
        else // Sinon
        {
            currentHealth += amount; // La vie actuel est égal à montant.
        }

        healthBar.SetHealth(currentHealth);// La barre de vie est égal à la vie actuel.
    }

    public void TakeDamage(int damage) // Variable pour la prise de dégats.
    {
        if (!isInvincible) //Si le joueur est invincible,
        {
            AudioManager.instance.PlayClipAt(hitSound,  transform.position);//On accède à l'audio manager afin de permettre de jouer un son
            currentHealth -= damage;//Le vie actuelle est égal à la vie actuelle moins les dégats reçus par le joueur.
            healthBar.SetHealth(currentHealth); // La barre de vie est égal à la vie actuel du joueur;

            if(currentHealth <= 0) // Si la vie du joueur est inferieur ou égal à 0 point de vie
            {
                Die(); //On fait mourir le personnage
                return;
            }

            isInvincible = true;
            StartCoroutine(InvincibilityFlash()); // On commence la corroutine "InvincibilityFlash".
            StartCoroutine(HandleInvincibilityDelay());// On commence la corroutine "HandleInvincibilityDela";
        }
    }

    public void Die()
    {
        PlayerMovement.instance.enabled = false; // Instance de mouvement de joueur activée est fausse
        PlayerMovement.instance.animator.SetTrigger("Die");
        PlayerMovement.instance.rb.bodyType = RigidbodyType2D.Kinematic; // Instance de mouvement du rigidbody du joueur activée est égal au rigidbody de type Kinematic.
        PlayerMovement.instance.rb.velocity = Vector3.zero; // Instance de mouvement de la velocité du rigidbody du joueur activée est égal au Vecteur 3.
        PlayerMovement.instance.playerCollider.enabled = false; // Instance de mouvement du collider du joueur activée est fausse.
        GameOverManager.instance.OnPlayerDeath();
    }

    public void Respawn()
    {
        PlayerMovement.instance.enabled = true;// Instance de mouvement de joueur activée est vrai
        PlayerMovement.instance.animator.SetTrigger("Respawn");
        PlayerMovement.instance.rb.bodyType = RigidbodyType2D.Dynamic;// Instance de mouvement du rigidbody du joueur activée est égal au rigidbody de type Dynamics.
        PlayerMovement.instance.playerCollider.enabled = true;// Instance de mouvement du collider du joueur activée est vrai.
        currentHealth = maxHealth; // La vie actuel est égal à la vie max.
        healthBar.SetHealth(currentHealth); //La barre de vie est égal à la vie actuel.
    }

    public IEnumerator InvincibilityFlash() // Varaible qui definit l'invincibilité flash du joueur.
    {
        while (isInvincible) // Tans que le joueur est invincible,
        {
            graphics.color = new Color(1f, 1f, 1f, 0f); // Les couleur graphics sont égals à 1 
            yield return new WaitForSeconds(invincibilityFlashDelay); //On attend quelques secondes.
            graphics.color = new Color(1f, 1f, 1f, 1f);// Les couleur graphics sont égals à 0
            yield return new WaitForSeconds(invincibilityFlashDelay); //On attend quelques secondes.
        }
    }

    public IEnumerator HandleInvincibilityDelay()
    {
        yield return new WaitForSeconds(invincibilityTimeAfterHit); //On attend quelques secondes.
        isInvincible = false; // Le joueur n'est pas invincible.
    }
}
