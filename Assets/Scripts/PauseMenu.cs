using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
  public static bool gameIsPaused = false; //Variable qui definit la pause du jeu.Par default, le jeu n'est pas en pause.
public GameObject pauseMenuUI;// Variable qui définit le menu de pause.
public GameObject settingWindows;
    void Update()
    {
      if(Input.GetKeyDown(KeyCode.Escape)) //Si la touche échappe est activé
      {
        if(gameIsPaused) //Et que on a le jeu en pose
        {
            Resume(); //On reconnance le jeu 
        }
        else //Sinon
        {
            Paused(); //On le met en pause
        }
      }  
    }
    void Paused()//Variable qui definit la pause du jeu
    {
      PlayerMovement.instance.enabled =  false; //On désactive tous les éléments du joueur dans la scène
      pauseMenuUI.SetActive(true);//On active le menu pause
      Time.timeScale = 0;//Le temps dans la scène est à 0 et tous est à l'arret.
      gameIsPaused = true;// Le jeu est bien en pause
    }
     public void Resume() //Variable qui permet de reprendre le cours du jeu
    {
      PlayerMovement.instance.enabled =  true; //On résactive tous les éléments du joueur dans la scène
      pauseMenuUI.SetActive(false);//On désactive le menu pause
      Time.timeScale = 1;//Le temps dans la scène est à 1 et tout est à la normal.
      gameIsPaused = false;// Le jeu n'est plus en pause
    }
    public void OpenSettingWindows()
    {
        settingWindows.SetActive(true);
    }
    public void CloseSettingWindows()
    {
        settingWindows.SetActive(false);
    }
    public void LoadMainMenu()
    {
      DontDestroyOnLoadScene.instance.RemoveFromDontDestroyOnLoad();//On enleve tous les éléments de la scène.
      Resume();//On fait appel à la fonction Reusme
      SceneManager.LoadScene("MainMenu");//On demarre le menu principale.
    }
}
