using UnityEngine;

public class WeakSpot : MonoBehaviour
{
    public GameObject objectToDestroy;
    public AudioClip killSound;

    private void OnTriggerEnter2D(Collider2D collision) //Variable qui permet de savoir si un objet entre en colision avec un autre.
    {
        if (collision.CompareTag("Player")) // Si le joueur entre en collision
        {
           AudioManager.instance.PlayClipAt(killSound, transform.position); //On accède à l'audio manager afin de permettre de jouer un son
           Destroy(objectToDestroy);// On détruit l'objet.
        }
    }
}
